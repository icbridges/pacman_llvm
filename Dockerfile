# Building Container
FROM debian:10-slim AS build

# Install all the llvm dependencies
RUN apt update
RUN apt -y install git cmake ninja-build binutils build-essential python

# Build LLVM from source
RUN git clone -b release/9.x --single-branch https://github.com/llvm/llvm-project.git
WORKDIR llvm-project

# Apply the PACMAN LLVM patch
COPY llvm/pacman_llvm.patch pacman_llvm.patch
RUN git apply pacman_llvm.patch

RUN mkdir build
WORKDIR build/
RUN cmake -G Ninja -DLLVM_ENABLE_PROJECTS="clang" -DLLVM_USE_LINKER=gold -DCMAKE_BUILD_TYPE=Release ../llvm
RUN ninja

# Install cross-compilation dependencies
WORKDIR /
RUN apt install -y gcc-multilib g++-multilib
RUN apt install -y gcc-aarch64-linux-gnu

# Compile the testing binaries
COPY tests tests
#WORKDIR tests
#RUN make all

# Testing Container
FROM debian:10-slim AS test

# Bring over the target
COPY --from=build tests tests

# Install QEMU build dependencies
RUN apt update
RUN apt -y install git bison flex wget

RUN sed -i 'p; s/deb /deb-src /' /etc/apt/sources.list
RUN apt update
RUN apt -y build-dep qemu

# Build custom version of QEMU (MTE support not yet in mainline)
RUN git clone --branch tgt-arm-mte-user --single-branch https://github.com/rth7680/qemu.git
WORKDIR qemu
RUN git submodule init
RUN git submodule update --recursive
RUN mkdir build
WORKDIR build
RUN ../configure --target-list=aarch64-linux-user
RUN make -j$(nproc)
RUN make install

# Run smoke tests
WORKDIR /tests
RUN chmod +x run.sh
